def unir(lista):
    hilera = ""
    for valor in lista:
        hilera = hilera + valor + ","
    hilera = hilera[:-1]
    return hilera

indiceFrom = 0
indiceTo = 1
currentFile = "hops.txt"

palabraClaveTo = "stops/"

otro = open(currentFile + "v2", "a")
f = open(currentFile, "r")

primeraLinea = True
for x in f:
    lineaLista = x.split(",")
    lineaLista[indiceFrom] = palabraClaveTo + lineaLista[indiceFrom]
    lineaLista[indiceTo] = palabraClaveTo + lineaLista[indiceTo]
    otro.write(unir(lineaLista))
    print('Se escribió una linea')
otro.close()